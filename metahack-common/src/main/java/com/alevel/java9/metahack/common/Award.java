package com.alevel.java9.metahack.common;

import java.util.Objects;

public class Award {
    private long id;
    private int place;
    private String award;
    private Team team;
    private Event event;

    public Award() {
    }

    public Award(long id, int place, String award, Team team, Event event) {
        this.id = id;
        this.place = place;
        this.award = award;
        this.team = team;
        this.event = event;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getAward() {
        return award;
    }

    public void setAward(String award) {
        this.award = award;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Award award1 = (Award) o;
        return id == award1.id &&
                place == award1.place &&
                Objects.equals(award, award1.award) &&
                Objects.equals(team, award1.team) &&
                Objects.equals(event, award1.event);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, place, award, team, event);
    }

    @Override
    public String toString() {
        return "Award{" +
                "id=" + id +
                ", place=" + place +
                ", award='" + award + '\'' +
                ", team=" + team +
                ", event=" + event +
                '}';
    }
}