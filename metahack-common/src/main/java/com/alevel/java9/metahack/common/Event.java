package com.alevel.java9.metahack.common;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

public class Event {
    private long id;
    private String theme;
    private String description;
    private String location;
    private int maxTeamsQty;
    private int maxTeamParticipantsQty;
    private ZonedDateTime startDate;
    private ZonedDateTime endDate;
    private User organizer;
    private List<Metadata> metadataList;
    private List<Project> projectList;
    private List<Award> awardList;
    private List<Team> teams;

    public Event() {
    }

    public Event(long id, String theme, String description, String location,
                 int maxTeamsQty, int maxTeamParticipantsQty, ZonedDateTime startDate,
                 ZonedDateTime endDate, List<Metadata> metadataList,
                 List<Project> projectList, List<Award> awardList, List<Team> teams) {
        this.id = id;
        this.theme = theme;
        this.description = description;
        this.location = location;
        this.maxTeamsQty = maxTeamsQty;
        this.maxTeamParticipantsQty = maxTeamParticipantsQty;
        this.startDate = startDate;
        this.endDate = endDate;
        this.metadataList = metadataList;
        this.projectList = projectList;
        this.awardList = awardList;
        this.teams = teams;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMaxTeamsQty() {
        return maxTeamsQty;
    }

    public void setMaxTeamsQty(int maxTeamsQty) {
        this.maxTeamsQty = maxTeamsQty;
    }

    public int getMaxTeamParticipantsQty() {
        return maxTeamParticipantsQty;
    }

    public void setMaxTeamParticipantsQty(int maxTeamParticipantsQty) {
        this.maxTeamParticipantsQty = maxTeamParticipantsQty;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public List<Metadata> getMetadataList() {
        return metadataList;
    }

    public void setMetadataList(List<Metadata> metadataList) {
        this.metadataList = metadataList;
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public List<Award> getAwardList() {
        return awardList;
    }

    public void setAwardList(List<Award> awardList) {
        this.awardList = awardList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public void setStartDateFromString(String date, String time){
        this.startDate = ZonedDateTime.of(LocalDate.parse(date), LocalTime.parse(time), ZonedDateTime.now().getZone());
    }

    public void setEndDateFromString(String date, String time){
        this.endDate = ZonedDateTime.of(LocalDate.parse(date), LocalTime.parse(time), ZonedDateTime.now().getZone());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return id == event.id &&
                maxTeamsQty == event.maxTeamsQty &&
                maxTeamParticipantsQty == event.maxTeamParticipantsQty &&
                Objects.equals(theme, event.theme) &&
                Objects.equals(description, event.description) &&
                Objects.equals(location, event.location) &&
                Objects.equals(startDate, event.startDate) &&
                Objects.equals(endDate, event.endDate) &&
                Objects.equals(organizer, event.organizer) &&
                Objects.equals(metadataList, event.metadataList) &&
                Objects.equals(projectList, event.projectList) &&
                Objects.equals(awardList, event.awardList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                theme,
                description,
                location,
                maxTeamsQty,
                maxTeamParticipantsQty,
                startDate,
                endDate,
                organizer,
                metadataList,
                projectList,
                awardList);
    }
}
