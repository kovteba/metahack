package com.alevel.java9.metahack.common;

import java.util.Objects;

public class Metadata {
    private long id;
    private String tagValue;
    private Tag tag;

    public Metadata() {
    }

    public Metadata(long id, String tagValue, Event event, Tag tag) {
        this.id = id;
        this.tagValue = tagValue;
        this.tag = tag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTagValue() {
        return tagValue;
    }

    public void setTagValue(String tagValue) {
        this.tagValue = tagValue;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Metadata metadata = (Metadata) o;
        return id == metadata.id && tagValue.equals(metadata.tagValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tagValue);
    }
}