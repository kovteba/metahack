package com.alevel.java9.metahack.common;

import java.util.Objects;

public class Project {
    private long id;
    private String name;
    private String description;
    private String sourceCodeLink;
    private Team team;
    private Event event;

    public Project() {
    }

    public Project(long id, String name, String description, String sourceCodeLink, Team team, Event event) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.sourceCodeLink = sourceCodeLink;
        this.event = event;
        this.team = team;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSourceCodeLink() {
        return sourceCodeLink;
    }

    public void setSourceCodeLink(String sourceCodeLink) {
        this.sourceCodeLink = sourceCodeLink;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return id == project.id &&
                Objects.equals(name, project.name) &&
                Objects.equals(description, project.description) &&
                Objects.equals(sourceCodeLink, project.sourceCodeLink) &&
                Objects.equals(team, project.team) &&
                Objects.equals(event, project.event);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, sourceCodeLink, team, event);
    }
}
