package com.alevel.java9.metahack.common.form;

public class TeamForm {

    private String nameTeam;

    private String token;

    public TeamForm(String nameTeam, String token) {
        this.nameTeam = nameTeam;
        this.token = token;
    }

    public String getNameTeam() {
        return nameTeam;
    }

    public void setNameTeam(String nameTeam) {
        this.nameTeam = nameTeam;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
