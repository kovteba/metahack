package com.alevel.java9.metahack.common;

import java.util.List;
import java.util.Objects;

public class Tag {
    private long id;
    private String name;
    private List<Metadata> metadataList;

    public Tag() {
    }

    public Tag(long id, String name, List<Metadata> metadataList) {
        this.id = id;
        this.name = name;
        this.metadataList = metadataList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Metadata> getMetadataList() {
        return metadataList;
    }

    public void setMetadataList(List<Metadata> metadataList) {
        this.metadataList = metadataList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return id == tag.id && name.equals(tag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
