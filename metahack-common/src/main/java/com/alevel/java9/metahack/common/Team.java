package com.alevel.java9.metahack.common;

import java.util.List;
import java.util.Objects;

public class Team {
    private long id;
    private String name;
    private User teamLeader;
    private List<User> members;
    private List<Event> eventList;
    private List<Award> awardList;
    private List<Project> projectList;

    public Team() {
    }

    public Team(long id, String name, List<User> members, List<Event> eventList, User teamLeader, List<Award> awardList) {
        this.id = id;
        this.name = name;
        this.members = members;
        this.eventList = eventList;
        this.teamLeader = teamLeader;
        this.awardList = awardList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    public User getTeamLeader() {
        return teamLeader;
    }

    public void setTeamLeader(User teamLeader) {
        this.teamLeader = teamLeader;
    }

    public List<Award> getAwardList() {
        return awardList;
    }

    public void setAwardList(List<Award> awardList) {
        this.awardList = awardList;
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return id == team.id &&
                Objects.equals(name, team.name) &&
                Objects.equals(teamLeader, team.teamLeader) &&
                Objects.equals(members, team.members) &&
                Objects.equals(eventList, team.eventList) &&
                Objects.equals(awardList, team.awardList) &&
                Objects.equals(projectList, team.projectList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, teamLeader, members, eventList, awardList, projectList);
    }
}
