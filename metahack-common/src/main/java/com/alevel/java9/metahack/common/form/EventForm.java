package com.alevel.java9.metahack.common.form;

public class EventForm {

    private String theme;
    private String description;
    private String location;
    private int maxTeamsQty;
    private int maxTeamParticipantsQty;

    private String startDate;
    private String startTime;

    private String finishDate;
    private String finishTime;

    private String token;

    public EventForm(String theme, String description,
                     String location, int maxTeamsQty,
                     int maxTeamParticipantsQty, String startDate,
                     String startTime, String finishDate,
                     String finishTime, String token) {
        this.theme = theme;
        this.description = description;
        this.location = location;
        this.maxTeamsQty = maxTeamsQty;
        this.maxTeamParticipantsQty = maxTeamParticipantsQty;
        this.startDate = startDate;
        this.startTime = startTime;
        this.finishDate = finishDate;
        this.finishTime = finishTime;
        this.token = token;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMaxTeamsQty() {
        return maxTeamsQty;
    }

    public void setMaxTeamsQty(int maxTeamsQty) {
        this.maxTeamsQty = maxTeamsQty;
    }

    public int getMaxTeamParticipantsQty() {
        return maxTeamParticipantsQty;
    }

    public void setMaxTeamParticipantsQty(int maxTeamParticipantsQty) {
        this.maxTeamParticipantsQty = maxTeamParticipantsQty;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "EventForm{" +
                "theme='" + theme + '\'' +
                ", description='" + description + '\'' +
                ", location='" + location + '\'' +
                ", maxTeamsQty=" + maxTeamsQty +
                ", maxTeamParticipantsQty=" + maxTeamParticipantsQty +
                ", startDate='" + startDate + '\'' +
                ", startTime='" + startTime + '\'' +
                ", finishDate='" + finishDate + '\'' +
                ", finishTime='" + finishTime + '\'' +
                '}';
    }
}
