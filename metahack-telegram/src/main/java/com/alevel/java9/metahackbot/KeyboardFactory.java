package com.alevel.java9.metahackbot;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

public class KeyboardFactory {

    public static InlineKeyboardMarkup getPagination(int pageQty, int currentPage) {
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();

        if (pageQty <= 5) {
            for (int i = 1; i <= pageQty; i++) {
                if (i == currentPage) {
                    row.add(new InlineKeyboardButton().setText("-" + i +"-")
                            .setCallbackData(Integer.toString(i)));
                } else {
                    row.add(new InlineKeyboardButton().setText(Integer.toString(i))
                            .setCallbackData(Integer.toString(i)));
                }
            }
        } else if (currentPage < 4) {
            for (int i = 1; i < 4; i++) {
                if (i == currentPage) {
                    row.add(new InlineKeyboardButton().setText("-" + i +"-")
                            .setCallbackData(Integer.toString(i)));
                } else {
                    row.add(new InlineKeyboardButton().setText(Integer.toString(i))
                            .setCallbackData(Integer.toString(i)));
                }
            }
            row.add(new InlineKeyboardButton().setText("4›")
                    .setCallbackData(Integer.toString(4)));
            row.add(new InlineKeyboardButton().setText(pageQty + "»")
                    .setCallbackData(Integer.toString(pageQty)));

        } else if (currentPage < pageQty - 2) {
            row.add(new InlineKeyboardButton().setText("«1").setCallbackData("1"));

            row.add(new InlineKeyboardButton().setText("‹" + (currentPage - 1))
                    .setCallbackData(Integer.toString(currentPage - 1)));
            row.add(new InlineKeyboardButton().setText("-" + currentPage + "-")
                    .setCallbackData(Integer.toString(currentPage)));
            row.add(new InlineKeyboardButton().setText(currentPage + 1 + "›")
                    .setCallbackData(Integer.toString(currentPage + 1)));
            row.add(new InlineKeyboardButton().setText(pageQty + "»")
                    .setCallbackData(Integer.toString(pageQty)));
        } else {
            row.add(new InlineKeyboardButton().setText("«1").setCallbackData("1"));

            row.add(new InlineKeyboardButton().setText("‹" + (pageQty - 3))
                    .setCallbackData(Integer.toString(pageQty - 3)));
            for (int i = pageQty - 2; i <= pageQty; i++) {
                if (i == currentPage) {
                    row.add(new InlineKeyboardButton().setText("-" + i +"-")
                            .setCallbackData(Integer.toString(i)));
                } else {
                    row.add(new InlineKeyboardButton().setText(Integer.toString(i))
                            .setCallbackData(Integer.toString(i)));
                }
            }
        }
        rowsInLine.add(row);

        InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();
        return keyboard.setKeyboard(rowsInLine);
    }

    public static ReplyKeyboardMarkup getUserContact() {
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        row.add(new KeyboardButton().setRequestContact(true).setText("Send my phone number"));
        keyboard.add(row);
        return new ReplyKeyboardMarkup().setKeyboard(keyboard).setResizeKeyboard(true)
                .setOneTimeKeyboard(true).setSelective(true);
    }
}
