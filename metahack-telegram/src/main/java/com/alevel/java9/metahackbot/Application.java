package com.alevel.java9.metahackbot;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

public class Application {

    public static void main(String[] args) {

        ApiContextInitializer.init();

        TelegramBotsApi botsApi = new TelegramBotsApi();

        try {
            MetahackBot metahackBot = new MetahackBot();
            botsApi.registerBot(metahackBot);
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }
}
