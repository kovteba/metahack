package com.alevel.java9.metahackbot.service;

import com.alevel.java9.metahack.common.Event;
import com.alevel.java9.metahack.common.Team;
import com.alevel.java9.metahack.common.User;
import com.alevel.java9.metahackbot.APIConnectionHelper;
import com.alevel.java9.metahackbot.KeyboardFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserEventsService {
    private static final int ENTRIES_PER_PAGE = 10;
    private static final Logger LOG = LoggerFactory.getLogger(StartService.class);
    private final MessageSender sender;
    private List<Event> events;
    private User user;
    private int currentPage = 1;
    private int pageQty;
    private long chatId;
    private final String ENDPOINT = "/users?phoneNumber=";

    public UserEventsService(MessageSender sender) {
        this.sender = sender;
    }

    public void replyToGetUserEvents(long chatId, String phoneNumber) {
        this.chatId = chatId;
        events = getEvents(phoneNumber);
        pageQty = (int) Math.ceil((double) events.size() / ENTRIES_PER_PAGE);
        try {
            SendMessage message = new SendMessage();
            if (events.isEmpty() || events == null) {
                message.setText("There is no upcoming events. You can register for event via website");
            } else {
                message.setText(getEventsMessage(currentPage));
                if (pageQty > 1) {
                    message.setReplyMarkup(KeyboardFactory.getPagination(pageQty, currentPage));
                }
            }
            message.enableHtml(true)
                    .setChatId(chatId);
            sender.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void replyToGetUserTeam(long chatId, String phoneNumber) {
        this.chatId = chatId;
        user = getUser(phoneNumber);
        SendMessage message = new SendMessage();
        String text = getTeamsMessage();
        try {
            if (text.isEmpty()) {
                message.setText("You are not a member of a team. Register your team first of contact your team leader");
            } else {
                message.setText(text);
            }
            message.setChatId(chatId)
                    .enableHtml(true);
            sender.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void replyToButtons(Long chatId, Integer messageId, String data) {
        try {
            currentPage = Integer.parseInt(data);
            sender.execute(new EditMessageText()
                    .setText(getEventsMessage(currentPage))
                    .enableHtml(true)
                    .setChatId(chatId)
                    .setMessageId(messageId)
                    .setReplyMarkup(KeyboardFactory.getPagination(pageQty, currentPage)));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private String getEventsMessage(int page) {
        StringBuilder sb = new StringBuilder();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy kk:mm");
        sb.append(String.format("Found: %s events\n\n", events.size()));
        for (int i = (page - 1) * ENTRIES_PER_PAGE; i < events.size() && i <= page * ENTRIES_PER_PAGE - 1; i++) {
            Event event = events.get(i);
            sb.append(String.format("<b>%s</b>\n", event.getTheme()))
                    .append(String.format("%s\n", event.getDescription()))
                    .append(String.format("Location: %s\n", event.getLocation()))
                    .append(String.format("Start date: %s\n", event.getStartDate().format(formatter)))
                    .append(String.format("End date: %s\n\n", event.getEndDate().format(formatter)));
        }
        return sb.toString();
    }

    private String getTeamsMessage() {
        StringBuilder sb = new StringBuilder();
        for (Team team : user.getTeamsList()) {
            sb.append(String.format("<b>%s</b>\n", team.getName()))
                    .append(String.format("Team leader: %s %s\n\n", team.getTeamLeader().getFirstName(), team.getTeamLeader().getLastName()));
            for (User member : team.getMembers()) {
                sb.append(String.format("%s %s\n", member.getFirstName(), member.getLastName()))
                        .append(String.format("%s\n", member.getPersonalInfo()))
                        .append(String.format("Email: %s\n", member.getEmail()))
                        .append(String.format("Phone number: %s\n\n", member.getPhoneNumber()));
            }
        }
        return sb.toString();
    }

    private List<Event> getEvents(String phoneNumber) {
        List<Event> events = new ArrayList<>();
        for (Team team : getUser(phoneNumber).getTeamsList()) {
            events.addAll(team.getEventList());
        }

        return events.stream().distinct().collect(Collectors.toList());
    }

    public User getUser(String phoneNumber) {
        String response = APIConnectionHelper.getRawJson(ENDPOINT + phoneNumber.substring(1));
        User user = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.registerModule(new JavaTimeModule());
            JsonNode node = mapper.readTree(response);
            user = mapper.treeToValue(node, User.class);
        } catch (JsonProcessingException e) {
            LOG.error(e.toString());
        }
        if (user == null) {
            try {
                SendMessage message = new SendMessage();

                message.setText("You are not registered user. Register on our website first.")
                        .setChatId(chatId);
                sender.execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
