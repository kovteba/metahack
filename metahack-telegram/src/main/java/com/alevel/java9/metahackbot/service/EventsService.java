package com.alevel.java9.metahackbot.service;

import com.alevel.java9.metahack.common.Event;
import com.alevel.java9.metahackbot.APIConnectionHelper;
import com.alevel.java9.metahackbot.KeyboardFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.time.format.DateTimeFormatter;

public class EventsService {

    private static final Logger LOG = LoggerFactory.getLogger(EventsService.class);
    private static final int ENTRIES_PER_PAGE = 10;
    private final static String UPCOMING_ENDPOINT = "/events/upcoming";
    private final static String BYTHEME_ENDPOINT = "/events/bytheme?theme=";
    private final MessageSender sender;
    private Event[] events;
    private int currentPage;
    private int pageQty;

    public EventsService(MessageSender sender) {
        this.sender = sender;
    }

    public void replyToGetEvents(long chatId) {
        events = getEvents(UPCOMING_ENDPOINT);
        composeResponse(chatId);
    }


    public void replyToGetEventsByTheme(Long chatId, String data) {
        events = getEvents(BYTHEME_ENDPOINT + data);
        composeResponse(chatId);
    }

    private void composeResponse(long chatId) {
        currentPage = 1;
        pageQty = (int) Math.ceil((double) events.length / ENTRIES_PER_PAGE);
        try {
            SendMessage message = new SendMessage();
            message.setText(getMessage(currentPage))
                    .setChatId(chatId)
                    .enableHtml(true);
            if (pageQty > 1) {
                message.setReplyMarkup(KeyboardFactory.getPagination(pageQty, currentPage));
            }
            sender.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void replyToButtons(Long chatId, Integer messageId, String data) {
        try {
            currentPage = Integer.parseInt(data);
            sender.execute(new EditMessageText()
                .setText(getMessage(currentPage))
                .enableHtml(true)
                .setChatId(chatId)
                .setMessageId(messageId)
                .setReplyMarkup(KeyboardFactory.getPagination(pageQty, currentPage)));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private String getMessage(int page) {
        StringBuilder sb = new StringBuilder();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy kk:mm");
        sb.append(String.format("Found: %s events\n\n", events.length));

        for (int i = (page - 1) * ENTRIES_PER_PAGE; i < events.length && i <= page * ENTRIES_PER_PAGE - 1; i++) {
            Event event = events[i];

            sb.append(String.format("<b>%s</b>\n", event.getTheme()))
                    .append(String.format("%s\n", event.getDescription()))
                    .append(String.format("Location: %s\n", event.getLocation()))
                    .append(String.format("Start date: %s\n", event.getStartDate().format(formatter)))
                    .append(String.format("End date: %s\n\n", event.getEndDate().format(formatter)));
        }
        return sb.toString();
    }

    private Event[] getEvents(String endpoint) {
        String response = APIConnectionHelper.getRawJson(endpoint);
        Event[] events = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.registerModule(new JavaTimeModule());
            JsonNode node = mapper.readTree(response);
            events = mapper.treeToValue(node, Event[].class);

        } catch (JsonProcessingException e) {
            LOG.error(e.toString());
        }
        return events;
    }
}
