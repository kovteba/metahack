package com.alevel.java9.metahackbot.service;

import com.alevel.java9.metahackbot.Constants;
import com.alevel.java9.metahackbot.KeyboardFactory;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class StartService {

    private final MessageSender sender;


    public StartService(MessageSender sender) {
        this.sender = sender;
    }

    public void replyToStart(long chatId) {
        try {
            SendMessage message = new SendMessage();
            message.setText(Constants.START_INFO)
                    .setChatId(chatId)
                    .setReplyMarkup(KeyboardFactory.getUserContact());
            sender.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
