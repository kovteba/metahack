package com.alevel.java9.metahackbot;

import com.alevel.java9.metahackbot.service.StartService;
import com.alevel.java9.metahackbot.service.EventsService;
import com.alevel.java9.metahackbot.service.UserEventsService;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.bot.DefaultAbilities;
import org.telegram.abilitybots.api.objects.Ability;
import org.telegram.abilitybots.api.objects.Flag;
import org.telegram.abilitybots.api.objects.Locality;
import org.telegram.abilitybots.api.objects.Privacy;

import java.util.Map;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static org.telegram.abilitybots.api.objects.Ability.builder;
import static org.telegram.abilitybots.api.objects.Flag.MESSAGE;
import static org.telegram.abilitybots.api.objects.Flag.REPLY;
import static org.telegram.abilitybots.api.objects.Locality.ALL;
import static org.telegram.abilitybots.api.objects.Privacy.PUBLIC;
import static org.telegram.abilitybots.api.util.AbilityMessageCodes.ABILITY_COMMANDS_NOT_FOUND;
import static org.telegram.abilitybots.api.util.AbilityUtils.getChatId;
import static org.telegram.abilitybots.api.util.AbilityUtils.getLocalizedMessage;

public class MetahackBot extends AbilityBot {

    private EventsService eventsService;
    private UserEventsService userEventsService;
    private StartService startService;
    private DefaultAbilities defaultAbilities;
    private final Map<Long, String> botUsers;
    private String phoneNumber;

    public MetahackBot() {
        super(Constants.BOT_TOKEN, Constants.BOT_NAME);
        botUsers = db.getMap(Constants.BOT_USERS);
        defaultAbilities = new DefaultAbilities(this);
        eventsService = new EventsService(sender);
        userEventsService = new UserEventsService(sender);
        startService = new StartService(sender);
    }

    @Override
    public int creatorId() {
        return Constants.CREATOR_ID;
    }

    public Ability startCommand() {
        return builder()
                .name("start")
                .info(Constants.START_DESCRIPTION)
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> startService.replyToStart(ctx.chatId()))
                .reply(upd -> {
                        silent.send("Now you can get personalized info", upd.getMessage().getChatId());
                        phoneNumber = upd.getMessage().getContact().getPhoneNumber();
                        botUsers.put(upd.getMessage().getChatId(), phoneNumber);
                    }, MESSAGE, REPLY)
                .build();
    }

    public Ability helpCommand() {
        return builder()
                .name("help")
                .info(Constants.HELP_DESCRIPTION)
                .locality(ALL)
                .privacy(PUBLIC)
                .input(0)
                .action(ctx -> {
                    String commands = "Available commands:\n";
                    commands += abilities().values().stream()
                            .filter(ability -> nonNull(ability.info()))
                            .map(ability -> {
                                String name = ability.name();
                                String info = ability.info();
                                return format("/%s - %s", name, info);
                            })
                            .sorted()
                            .reduce((a, b) -> format("%s%n%s", a, b))
                            .orElse(getLocalizedMessage(ABILITY_COMMANDS_NOT_FOUND, ctx.user().getLanguageCode()));

                    silent.send(commands, ctx.chatId());
                })
                .build();
    }

    public Ability getUpcomingEvents() {
        return Ability
                .builder()
                .name("upcoming")
                .info(Constants.UPCOMING_DESCRIPTION)
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx ->  eventsService.replyToGetEvents(ctx.chatId()))
                .reply(upd -> eventsService
                        .replyToButtons(getChatId(upd), upd.getCallbackQuery().getMessage().getMessageId(),
                                upd.getCallbackQuery().getData()),
                        Flag.CALLBACK_QUERY)
                .build();
    }


    public Ability getEventsByTheme() {
        return Ability
                .builder()
                .name("bytheme")
                .info(Constants.BY_THEME_DESCRIPTION)
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx -> silent.send("Enter event theme to search for", ctx.chatId()))
                .reply(upd -> eventsService.replyToGetEventsByTheme(getChatId(upd),
                        upd.getCallbackQuery().getData()),
                        Flag.CALLBACK_QUERY)
                .build();
    }

    public Ability getUserEvents() {
        return Ability
                .builder()
                .name("myevents")
                .info(Constants.USER_EVENTS_DESCRIPTION)
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx -> {
                    if (botUsers.containsKey(ctx.chatId())) {
                        userEventsService.replyToGetUserEvents(ctx.chatId(), botUsers.get(ctx.chatId()));
                    } else {
                        silent.send("Provide me with your contact info, first enter " +
                                "/start command and then press 'Send my phone number' button", ctx.chatId());
                    }
                })
                .reply(upd -> userEventsService
                                .replyToButtons(getChatId(upd), upd.getCallbackQuery().getMessage().getMessageId(),
                                        upd.getCallbackQuery().getData()),
                        Flag.CALLBACK_QUERY)
                .build();
    }

    public Ability getUserTeams() {
        return Ability
                .builder()
                .name("team")
                .info(Constants.USER_TEAM_DESCRIPTION)
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx -> {
                    if (botUsers.containsKey(ctx.chatId())) {
                        userEventsService.replyToGetUserTeam(ctx.chatId(), botUsers.get(ctx.chatId()));
                    } else {
                        silent.send("Provide me with your contact info, first enter " +
                                "/start command and then press 'Send my phone number' button", ctx.chatId());
                    }
                })
                .build();
    }
}
