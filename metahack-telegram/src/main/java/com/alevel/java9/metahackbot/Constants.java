package com.alevel.java9.metahackbot;

public interface Constants {

    String BOT_TOKEN = "1102129688:AAExKAxwEnmFwZZ0js7jfRzpQ-G8hZyyaz0";
    String BOT_NAME = "Metahack";
    int CREATOR_ID = 306758130;

    String API_URL = "http://localhost:8081";

    String START_INFO = "Use this bot to obtain information about hackathons.\n" +
            "For getting personalized information, please provide access to your phone number.";
    String START_DESCRIPTION = "start using the metahack bot";
    String HELP_DESCRIPTION = "list all known commands";
    String UPCOMING_DESCRIPTION = "show upcoming events";
    String USER_EVENTS_DESCRIPTION = "show events you are participating in";
    String USER_TEAM_DESCRIPTION = "show info about your team members";
    String BY_THEME_DESCRIPTION = "find events by theme name";
    String BOT_USERS = "BOT_USERS";
}
