package com.alevel.java9.metahackbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class APIConnectionHelper {
    private static final Logger LOG = LoggerFactory.getLogger(APIConnectionHelper.class);

    public static String getRawJson(String endpoint) {
        String urlString = Constants.API_URL + endpoint;
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            LOG.error(e.toString());
        }
        return response.toString();
    }
}
