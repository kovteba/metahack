INSERT INTO roles (id, name) VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_TEAM_LEADER'),
(3, 'ROLE_ORGANIZER');

INSERT INTO users (email, first_name, last_name, login, password, personal_info, phone_number, role_id) VALUES
('xyz@email.com', 'John', 'Doe', 'john-doe', 'rootpwd', 'senior java developer', '380937894561', 2),
('grigoriy.belov@gmail.com', 'Grigoriy', 'Belov', 'grigoriy-belov', 'root', 'java trainee', '380669169585', 1),
('user1@email.com', 'Vasya', 'Pupkin', 'vasya-pupkin', 'user1', 'senior principal solution architect', '380937894523', 3),
('user2@email.com', 'Bill', 'Gates', 'bill-gates', 'user1', 'js developer', '380937894554', 1),
('user3@email.com', 'Harry', 'Hacker', 'harry-hacker', 'user1', 'senior c# developer', '380937894563', 2);

INSERT INTO teams (name, team_leader_id) VALUES
('Black Team', '1'),
('Red Team', '5');

INSERT INTO users_teams (user_id, team_id) VALUES
('1', '1'),
('2', '1'),
('4', '2'),
('5', '2'),
('2', '2');

INSERT INTO events (description, end_date, location, max_team_participants_qty, max_teams_qty, start_date, theme, organizer_id) VALUES
('DescriptionTest', '2020-03-31 20:00:00', 'Kharkov, A-level Ukraine office', 5, 10, '2020-03-30 9:30:00', 'Metahack', 3),
('DescriptionTest2', '2019-03-31 20:00:00', 'Kharkov, EPAM office', 5, 10, '2019-03-30 9:30:00', 'HackIT', 3),
('DescriptionTest3', '2020-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2020-04-29 9:30:00', 'ThemeTest3', 3),
('DescriptionTest4', '2019-06-30 20:00:00', 'Dnepr, SoftServe office', 5, 10, '2019-06-29 9:30:00', 'ThemeTest4', 3),
('DescriptionTest5', '2020-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2020-04-29 9:30:00', 'ThemeTest5', 3),
('DescriptionTest6', '2020-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2020-04-29 9:30:00', 'ThemeTest6', 3),
('DescriptionTest7', '2020-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2020-04-29 9:30:00', 'ThemeTest7', 3),
('DescriptionTest8', '2020-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2020-04-29 9:30:00', 'ThemeTest8', 3),
('DescriptionTest9', '2020-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2020-04-29 9:30:00', 'ThemeTest9', 3),
('DescriptionTest10', '2019-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2019-04-29 9:30:00', 'ThemeTest10', 3),
('DescriptionTest11', '2019-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2019-04-29 9:30:00', 'ThemeTest11', 3),
('DescriptionTest12', '2019-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2019-04-29 9:30:00', 'ThemeTest12', 3),
('DescriptionTest13', '2019-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2019-04-29 9:30:00', 'ThemeTest13', 3),
('DescriptionTest14', '2019-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2019-04-29 9:30:00', 'ThemeTest14', 3),
('DescriptionTest15', '2019-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2019-04-29 9:30:00', 'ThemeTest15', 3),
('DescriptionTest16', '2019-04-30 20:00:00', 'Kiev, Global Logic office', 5, 10, '2019-04-29 9:30:00', 'ThemeTest16', 3);

INSERT INTO events_teams ("event_id", "team_id") VALUES
(2, 1),
(2, 2),
(4, 1),
(4, 2);

INSERT INTO tags (name) VALUES
('sponsors');

INSERT INTO metadata (tag_value, event_id, tag_id) VALUES
('A-level', 1, 1),
('Art Joker', 1, 1),
('EPAM', 2, 1),
('GlobalLogic', 3, 1),
('SoftServe', 4, 1);

INSERT INTO projects ("description", "name", "source_code_link", "event_id", "team_id") VALUES
('Test project description', 'Test project', 'https://github.com/testproject', 2, 1),
('Test project2 description', 'Test project2', 'https://github.com/testproject2', 2, 2),
('Test project3 description', 'Test project3', 'https://github.com/testproject3', 4, 1),
('Test project4 description', 'Test project4', 'https://github.com/testproject4', 4, 2);

INSERT INTO awards ("award", "place", "event_id", "team_id") VALUES
('1000$', 1, 2, 2),
('200$', 2, 2, 1),
('3000$', 1, 4, 1),
('1000$', 2, 4, 2);
