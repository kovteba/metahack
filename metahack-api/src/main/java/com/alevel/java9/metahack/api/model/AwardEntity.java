package com.alevel.java9.metahack.api.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "awards")
public class AwardEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Integer place;

    @Column
    private String award;

    @ManyToOne (optional = false, cascade = CascadeType.ALL)
    @JoinColumn (name = "team_id")
    private TeamEntity team;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "event_id")
    private EventEntity event;
}
