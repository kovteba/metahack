package com.alevel.java9.metahack.api.controller;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.mapper.AwardMapper;
import com.alevel.java9.metahack.api.model.AwardEntity;
import com.alevel.java9.metahack.api.service.AwardServiceImpl;
import com.alevel.java9.metahack.common.Award;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/awards")
@AllArgsConstructor
public class AwardController {

    private final AwardServiceImpl awardService;
    private final AwardMapper awardMapper;

    @GetMapping
    public List<Award> getAllAwards() {

        return awardService.getAllAwards()
                .stream()
                .map(awardMapper::awardEntityToAward)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Award getAwardById(@PathVariable("id") Long id)
            throws RecordNotFoundException {

        return awardMapper.awardEntityToAward(awardService.getAwardById(id));
    }

    @PostMapping("/{teamId}/{eventId}")
    public ResponseEntity<Award> createAward(@RequestBody Award award, @PathVariable Long teamId,
                                             @PathVariable Long eventId) throws RecordNotFoundException {

        AwardEntity created = awardService.createAward(awardMapper.awardToAwardEntity(award), teamId, eventId);

        return ResponseEntity.status(HttpStatus.CREATED).body(awardMapper.awardEntityToAward(created));
    }

    @DeleteMapping("/{id}")
    public void deleteAwardById(@PathVariable("id") Long id)
            throws RecordNotFoundException {

        awardService.deleteAwardById(id);
    }
}