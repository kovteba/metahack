package com.alevel.java9.metahack.api.mapper;

import com.alevel.java9.metahack.api.model.ProjectEntity;
import com.alevel.java9.metahack.common.Project;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ProjectMapper {

    Project projectEntityToProject(ProjectEntity projectEntity);

    ProjectEntity projectToProjectEntity(Project project);
}
