package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.TagEntity;
import com.alevel.java9.metahack.api.repository.TagRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Override
    public TagEntity getTagById(Long id) throws RecordNotFoundException {
        return tagRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No tag record exist for given id"));
    }
}
