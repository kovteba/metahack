package com.alevel.java9.metahack.api.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "tokens")
public class TokenEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "token")
    private String token;

    @Column(name = "user_id")
    private Long userId;

    public TokenEntity() {
    }

    public TokenEntity(String token, Long userId) {
        this.token = token;
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenEntity that = (TokenEntity) o;
        return token.equals(that.token) &&
                userId.equals(that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, userId);
    }
}
