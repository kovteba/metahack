package com.alevel.java9.metahack.api.mapper;

import com.alevel.java9.metahack.api.model.MetadataEntity;
import com.alevel.java9.metahack.common.Metadata;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface MetadataMapper {

    Metadata metadataEntityToMetadata(MetadataEntity metadataEntity);

    @InheritInverseConfiguration
    MetadataEntity metadataToMetadataEntity(Metadata metadata);
}
