package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.TokenEntity;

public interface TokenService {

    TokenEntity getTokenByToken(String token) throws RecordNotFoundException;

    TokenEntity createToken(TokenEntity tokenEntity) throws RecordNotFoundException;

    void deleteToken(String token) throws RecordNotFoundException;

}
