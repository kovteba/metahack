package com.alevel.java9.metahack.api.mapper;

import com.alevel.java9.metahack.api.model.TokenEntity;
import com.alevel.java9.metahack.common.Token;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface TokenMapper {

    Token tokenEntityToToken(TokenEntity tokenEntity);

    TokenEntity tokenToTokenEntity(Token token);

}
