package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.UserEntity;

import java.util.List;

public interface UserService {

    UserEntity getUserById(Long id) throws RecordNotFoundException;

    UserEntity createUser(UserEntity entity);

    UserEntity updateUser(UserEntity entity, Long id) throws RecordNotFoundException;

    void deleteUserById(Long id) throws RecordNotFoundException;

    UserEntity findUserEntityByEmail(String email);

    UserEntity findUserEntityByPhoneNumber(String phoneNumber);
    
    List<UserEntity> getAllUsers();
    
}
