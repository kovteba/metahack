package com.alevel.java9.metahack.api.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "events")
public class EventEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String theme;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    @Column
    private String location;

    @Column
    private Integer maxTeamsQty;

    @Column
    private Integer maxTeamParticipantsQty;

    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime startDate;

    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime endDate;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "events_teams",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id")
    )
    private List<TeamEntity> teams;

    @OneToMany (mappedBy = "event")
    private List<AwardEntity> awards;

    @OneToMany (mappedBy = "event")
    private List<MetadataEntity> metadata;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "organizer_id")
    private UserEntity organizer;

    @OneToMany(mappedBy = "event")
    private List<ProjectEntity> projects;

    public void addTeam(TeamEntity team) {
        this.teams.add(team);
    }
}
