package com.alevel.java9.metahack.api.controller;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.mapper.TokenMapper;
import com.alevel.java9.metahack.api.model.TokenEntity;
import com.alevel.java9.metahack.api.service.TokenServiceImpl;
import com.alevel.java9.metahack.common.Token;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/tokens")
@AllArgsConstructor
public class TokenController {

    private final TokenServiceImpl TOKEN_SERVICE;

    private final TokenMapper TOKEN_MAPPER;

    @GetMapping
    public Object getTokenByToken(@RequestHeader String token) throws RecordNotFoundException {
        if (TOKEN_SERVICE.getTokenByToken(token) == null){
            return new Token("0", 0L);
        } else {
            return TOKEN_MAPPER.tokenEntityToToken(TOKEN_SERVICE.getTokenByToken(token));
        }

    }

    @PostMapping
    public TokenEntity createToken(@RequestBody Token token) throws RecordNotFoundException {
        return TOKEN_SERVICE.createToken(TOKEN_MAPPER.tokenToTokenEntity(token));
    }

    @PostMapping("/delete")
    @Transactional
    void deleteToken(@RequestBody String token) throws RecordNotFoundException {
        TOKEN_SERVICE.deleteToken(token);
    }
}
