package com.alevel.java9.metahack.api.mapper;

import com.alevel.java9.metahack.api.model.TagEntity;
import com.alevel.java9.metahack.common.Tag;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface TagMapper {

    Tag tagEntityToTag(TagEntity tagEntity);

    @InheritInverseConfiguration
    TagEntity tagToTagEntity(Tag tag);
}
