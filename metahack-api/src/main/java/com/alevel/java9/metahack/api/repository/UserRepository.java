package com.alevel.java9.metahack.api.repository;

import com.alevel.java9.metahack.api.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByEmail(String email);

    UserEntity findByPhoneNumber(String phoneNumber);

}
