package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.UserEntity;
import com.alevel.java9.metahack.api.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository USER_REPOSITORY;

    @Override
    public UserEntity getUserById(Long id) throws RecordNotFoundException {
        Optional<UserEntity> user = USER_REPOSITORY.findById(id);

        return user.orElseThrow(() -> new RecordNotFoundException("No user record exist for given id"));
    }

    @Override
    public UserEntity createUser(UserEntity entity) {
        return USER_REPOSITORY.save(entity);
    }

    @Override
    @Transactional
    public UserEntity updateUser(UserEntity entity, Long id) throws RecordNotFoundException {
        USER_REPOSITORY.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No user record exist for given id"));

        return USER_REPOSITORY.save(entity);
    }

    @Override
    @Transactional
    public void deleteUserById(Long id) throws RecordNotFoundException {
        USER_REPOSITORY.findById(id).orElseThrow(() -> new RecordNotFoundException("No user record exist for given id"));

        USER_REPOSITORY.deleteById(id);
    }

    @Override
    public UserEntity findUserEntityByEmail(String email) {
        return USER_REPOSITORY.findByEmail(email);
    }

    @Override
    public UserEntity findUserEntityByPhoneNumber(String phoneNumber) {
        return USER_REPOSITORY.findByPhoneNumber(phoneNumber);
    }
    
    @Override
    public List<UserEntity> getAllUsers() {
        return USER_REPOSITORY.findAll();
    }
    
}
