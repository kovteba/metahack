package com.alevel.java9.metahack.api.mapper;

import com.alevel.java9.metahack.api.model.TeamEntity;
import com.alevel.java9.metahack.common.Team;
import org.mapstruct.*;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {EventMapper.class, AwardMapper.class, ProjectMapper.class})
public interface TeamMapper {

    @Mappings({
            @Mapping(source = "events", target = "eventList"),
            @Mapping(source = "awards", target = "awardList"),
            @Mapping(source = "projects", target = "projectList")
    })
    Team teamEntityToTeam(TeamEntity teamEntity);

    TeamEntity teamToTeamEntity(Team team);
}
