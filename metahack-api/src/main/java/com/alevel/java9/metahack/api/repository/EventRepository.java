package com.alevel.java9.metahack.api.repository;

import com.alevel.java9.metahack.api.model.EventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.ZonedDateTime;
import java.util.List;

public interface EventRepository extends JpaRepository<EventEntity, Long> {

    List<EventEntity> findEventEntitiesByStartDateAfter(ZonedDateTime currentDate);

    List<EventEntity> findEventEntitiesByStartDateBefore(ZonedDateTime currentDate);

    List<EventEntity> findEventEntitiesByThemeContainsIgnoreCase(String theme);

    EventEntity findEventEntityById(Long id);
    
    List<EventEntity> findAllByOrganizerId(Long id);

}
