package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.EventEntity;

import java.util.List;

public interface EventService {

    EventEntity getEventById(Long id) throws RecordNotFoundException;

    EventEntity createEvent(EventEntity eventEntity);

    EventEntity updateEventById(EventEntity eventEntity, Long id) throws RecordNotFoundException;

    List<EventEntity> getAllEvents();

    List<EventEntity> returnEventStartLessTwoDay();

    List<EventEntity> getUpcomingEvents();

    List<EventEntity> getPastEvents();

    List<EventEntity> getEventsByTheme(String theme);

    void registerForEvent(Long eventId, Long id) throws RecordNotFoundException;

    List<EventEntity> getEventsByUserId(Long id) throws RecordNotFoundException ;
    
    List<EventEntity> getAllEventsByOrganaizer(Long id);
    
}