package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.RoleEntity;

public interface RoleService {
    RoleEntity getRoleByName(String name) throws RecordNotFoundException;

    RoleEntity getRoleById(Integer roleId) throws RecordNotFoundException;
}
