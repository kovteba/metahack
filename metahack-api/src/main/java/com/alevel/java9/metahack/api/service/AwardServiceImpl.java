package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.AwardEntity;
import com.alevel.java9.metahack.api.model.EventEntity;
import com.alevel.java9.metahack.api.model.TeamEntity;
import com.alevel.java9.metahack.api.repository.AwardRepository;
import com.alevel.java9.metahack.api.repository.EventRepository;
import com.alevel.java9.metahack.api.repository.TeamRepository;
import com.alevel.java9.metahack.common.Event;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class AwardServiceImpl implements AwardService {

    private final AwardRepository awardRepository;
    private final TeamRepository teamRepository;
    private final EventRepository eventRepository;

    @Override
    public List<AwardEntity> getAllAwards() {
        return new ArrayList<>(awardRepository.findAll());
    }

    @Override
    public AwardEntity getAwardById(Long id) throws RecordNotFoundException {
        return awardRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No award record exist for given id"));
    }

    @Override
    @Transactional
    public AwardEntity createAward(AwardEntity award, Long teamId, Long eventId) throws RecordNotFoundException {

        TeamEntity team = teamRepository.findById(teamId)
                .orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));
        EventEntity event = eventRepository.findById(eventId)
                .orElseThrow(() -> new RecordNotFoundException("No event record exist for given id"));

        award.setTeam(team);
        award.setEvent(event);
        return awardRepository.save(award);
    }

    @Override
    @Transactional
    public void deleteAwardById(Long id) throws RecordNotFoundException {
        awardRepository.findById(id).orElseThrow(() -> new RecordNotFoundException("No award record exist for given id"));

        awardRepository.deleteById(id);
    }

}