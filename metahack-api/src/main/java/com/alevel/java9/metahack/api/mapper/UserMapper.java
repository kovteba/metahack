package com.alevel.java9.metahack.api.mapper;

import com.alevel.java9.metahack.api.model.UserEntity;
import com.alevel.java9.metahack.common.User;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = TeamMapper.class)
public interface UserMapper {

    @Mapping(source = "teams", target = "teamsList")
    User userEntityToUser(UserEntity userEntity);

    @InheritInverseConfiguration
    UserEntity userToUserEntity(User user);
}
