package com.alevel.java9.metahack.api.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "teams")
public class TeamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToOne
    @JoinColumn(name = "team_leader_id")
    private UserEntity teamLeader;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_teams",
            joinColumns = { @JoinColumn(name = "team_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    private List<UserEntity> members;

    @ManyToMany(mappedBy = "teams")
    private List<EventEntity> events;

    @OneToMany (mappedBy = "team")
    private List<AwardEntity> awards;

    @OneToMany(mappedBy = "team")
    private List<ProjectEntity> projects;

    public TeamEntity() {
    }

    public TeamEntity(String name) {
        this.name = name;
    }

    public void addMembers(List<UserEntity> members) {
        this.members.addAll(members);
    }

    public void removeMembers(List<UserEntity> members) {
        this.members.removeAll(members);
    }

    public void addEvent(EventEntity event) {
        this.events.add(event);
    }
}
