package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.EventEntity;
import com.alevel.java9.metahack.api.model.TeamEntity;
import com.alevel.java9.metahack.api.model.UserEntity;
import com.alevel.java9.metahack.api.repository.EventRepository;
import com.alevel.java9.metahack.api.repository.TeamRepository;
import com.alevel.java9.metahack.api.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;
    private final TeamRepository teamRepository;
    private final UserRepository userRepository;

    @Override
    public EventEntity getEventById(Long id) throws RecordNotFoundException {
        Optional<EventEntity> eventEntity = eventRepository.findById(id);
        return eventEntity.orElseThrow(
                () ->  new RecordNotFoundException("No event record exist for given id : " + id));
    }

    @Override
    public EventEntity createEvent(EventEntity eventEntity) {
        return eventRepository.save(eventEntity);
    }

    @Override
    @Transactional
    public EventEntity updateEventById(EventEntity eventEntity, Long id) throws RecordNotFoundException {
        eventRepository.findById(id).orElseThrow(() -> new RecordNotFoundException("No event record exist for given id : " + id));
        return eventRepository.save(eventEntity);
    }

    @Override
    public List<EventEntity> getAllEvents() {
        return eventRepository.findAll();
    }

    @Override
    public List<EventEntity> returnEventStartLessTwoDay() {
        List<EventEntity> listEvent = eventRepository.findAll();
        List<EventEntity> listResult = new ArrayList<>();
        for (EventEntity eventEntity : listEvent){
            if (eventEntity.getStartDate().toLocalDate().minusDays(1)
                    .equals(ZonedDateTime.now().toLocalDate())){
                listResult.add(eventEntity);
            }
        }
        return listResult;
    }

    @Override
    public List<EventEntity> getUpcomingEvents() {
        return eventRepository.findEventEntitiesByStartDateAfter(ZonedDateTime.now());
    }

    @Override
    public List<EventEntity> getPastEvents() {
        return eventRepository.findEventEntitiesByStartDateBefore(ZonedDateTime.now());
    }

    @Override
    public List<EventEntity> getEventsByTheme(String theme) {
        return eventRepository.findEventEntitiesByThemeContainsIgnoreCase(theme.toLowerCase());
    }

    @Override
    @Transactional
    public void registerForEvent(Long eventId, Long id) throws RecordNotFoundException {
        EventEntity updatedEvent = eventRepository.findById(eventId)
                .orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));

        TeamEntity registeredTeam = teamRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));

        updatedEvent.addTeam(registeredTeam);
        eventRepository.save(updatedEvent);
    }

    @Override
    @Transactional
    public List<EventEntity> getEventsByUserId(Long id) throws RecordNotFoundException {
        UserEntity user = userRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No user record exist for given id"));
        List<Long> ids = user.getTeams().stream().map(TeamEntity::getId).collect(Collectors.toList());
        List<TeamEntity> teams = teamRepository.findAllById(ids);
        List<EventEntity> events = new ArrayList<>();
        teams.forEach(t -> events.addAll(t.getEvents()));
        return events;
    }
    
    @Override
    public List<EventEntity> getAllEventsByOrganaizer(Long id) {
        return eventRepository.findAllByOrganizerId(id);
    }
    
}
