package com.alevel.java9.metahack.api.controller;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.mapper.EventMapper;
import com.alevel.java9.metahack.api.model.EventEntity;
import com.alevel.java9.metahack.api.service.EventService;
import com.alevel.java9.metahack.api.service.UserService;
import com.alevel.java9.metahack.common.Event;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/events")
@AllArgsConstructor
public class EventController {

    private final UserService userService;
    private final EventService eventService;
    private final EventMapper eventMapper;

    @GetMapping
    public List<Event> getAllEvents() {
        return eventService.getAllEvents()
                .stream()
                .map(eventMapper::eventEntityToEvent)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Event getEventById(@PathVariable Long id) throws RecordNotFoundException {
        return  eventMapper.eventEntityToEvent(eventService.getEventById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<Event> createEvent(@RequestBody Event event) throws RecordNotFoundException {
        EventEntity eventEntity = eventMapper.eventToEventEntity(event);
        EventEntity created = eventService.createEvent(eventEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(eventMapper.eventEntityToEvent(created));
    }

    @GetMapping("/lessTwoDay")
    public List<Event> eventLessTwoDay() {
        return eventService.returnEventStartLessTwoDay()
                .stream()
                .map(eventMapper::eventEntityToEvent)
                .collect(Collectors.toList());
    }

    @GetMapping("/upcoming")
    public List<Event> getUpcomingEvents() {
        return eventService.getUpcomingEvents().stream()
                .map(eventMapper::eventEntityToEvent)
                .collect(Collectors.toList());
    }

    @GetMapping("/past")
    public List<Event> getPastEvents() {
        return eventService.getPastEvents().stream()
                .map(eventMapper::eventEntityToEvent)
                .collect(Collectors.toList());
    }

    @GetMapping("/bytheme")
    public List<Event> getEventsByTheme(@RequestParam String theme) {
        return eventService.getEventsByTheme(theme).stream()
                .map(eventMapper::eventEntityToEvent)
                .collect(Collectors.toList());
    }

    @PostMapping("/{id}/teams")
    public void registerForEvent(@RequestBody Long eventId, @PathVariable Long id)
            throws RecordNotFoundException {
        eventService.registerForEvent(eventId, id);
    }

    @GetMapping("/users/{id}")
    public List<Event> getEventsByUserId(@PathVariable Long id) throws RecordNotFoundException {
        return eventService.getEventsByUserId(id).stream()
                .map(eventMapper::eventEntityToEvent)
                .collect(Collectors.toList());
    }
    
    @GetMapping("/user/{id}")
    public List<Event> getEventsByOrganizerId(@PathVariable Long id) throws RecordNotFoundException {
        return eventService.getAllEventsByOrganaizer(id).stream()
                .map(eventMapper::eventEntityToEvent)
                .collect(Collectors.toList());
    }
    
}
