package com.alevel.java9.metahack.api.repository;

import com.alevel.java9.metahack.api.model.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<TagEntity, Long> {
}
