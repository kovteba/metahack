package com.alevel.java9.metahack.api.controller;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.mapper.UserMapper;
import com.alevel.java9.metahack.api.model.RoleEntity;
import com.alevel.java9.metahack.api.model.UserEntity;
import com.alevel.java9.metahack.api.service.RoleService;
import com.alevel.java9.metahack.api.service.UserService;
import com.alevel.java9.metahack.common.User;
import com.alevel.java9.metahack.common.form.LoginForm;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private final UserService userService;
    private final RoleService roleService;
    private final UserMapper userMapper;

    @GetMapping("/{id}")
    public User getUserById(@PathVariable("id") Long id)
            throws RecordNotFoundException {

        return userMapper.userEntityToUser(userService.getUserById(id));
    }

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user) throws RecordNotFoundException {

        RoleEntity roleEntity = roleService.getRoleByName("ROLE_USER");
        UserEntity userEntity = userMapper.userToUserEntity(user);
        userEntity.setRole(roleEntity);
        UserEntity created = userService.createUser(userEntity);

        return ResponseEntity.status(HttpStatus.CREATED).body(userMapper.userEntityToUser(created));
    }

    @PostMapping(value = "/login")
    public ResponseEntity<Object> pageUser(@RequestBody LoginForm loginForm) {
        UserEntity user = userService.findUserEntityByEmail(loginForm.getEmail());
        return ResponseEntity.status(HttpStatus.CREATED).body(userMapper.userEntityToUser(user));
    }

    @PatchMapping("/priviledged/{id}")
    public User promoteUser(@RequestBody Integer roleId, @PathVariable Long id)
            throws RecordNotFoundException {
        RoleEntity roleEntity = roleService.getRoleById(roleId);
        UserEntity userEntity = userService.getUserById(id);
        userEntity.setRole(roleEntity);

        UserEntity updated = userService.updateUser(userEntity, id);

        return userMapper.userEntityToUser(updated);
    }

    @PutMapping("/{id}")
    public User updateUser(@RequestBody User user, @PathVariable Long id)
            throws RecordNotFoundException {

        UserEntity updated = userService.updateUser(userMapper.userToUserEntity(user), id);

        return userMapper.userEntityToUser(updated);
    }

    @DeleteMapping("/{id}")
    public void deleteUserById(@PathVariable("id") Long id)
            throws RecordNotFoundException {

        userService.deleteUserById(id);
    }

    @GetMapping
    public User getUserByPhoneNumber(@RequestParam("phoneNumber") String phoneNumber)
        throws RecordNotFoundException {

        return userMapper.userEntityToUser(userService.findUserEntityByPhoneNumber(phoneNumber));
    }
    
    @RequestMapping("/all")
    public List<User> getAllUsers(){
        return userService.getAllUsers()
                .stream()
                .map(userMapper::userEntityToUser)
                .collect(Collectors.toList());
    }
}
