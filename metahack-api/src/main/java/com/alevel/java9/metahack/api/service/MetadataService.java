package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.MetadataEntity;

public interface MetadataService {

    MetadataEntity getMetadataById(Long id) throws RecordNotFoundException;
}
