package com.alevel.java9.metahack.api.repository;

import com.alevel.java9.metahack.api.model.MetadataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MetadataRepository extends JpaRepository<MetadataEntity, Long> {

}
