package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.RoleEntity;
import com.alevel.java9.metahack.api.repository.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public RoleEntity getRoleByName(String name) throws RecordNotFoundException {
        Optional<RoleEntity> role = roleRepository.findByName(name);

        return role.orElseThrow(() -> new RecordNotFoundException("No role record exist for given name"));
    }

    @Override
    public RoleEntity getRoleById(Integer roleId) throws RecordNotFoundException {
        Optional<RoleEntity> role = roleRepository.findById(roleId);

        return role.orElseThrow(() -> new RecordNotFoundException("No role record exist for given name"));
    }
}
