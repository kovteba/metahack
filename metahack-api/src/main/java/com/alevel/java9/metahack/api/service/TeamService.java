package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.TeamEntity;

import java.util.List;

public interface TeamService {
    
    List<TeamEntity> getAllTeams();

    TeamEntity getTeamById(Long id) throws RecordNotFoundException;

    TeamEntity createTeam(TeamEntity entity);

    void deleteTeamById(Long id) throws RecordNotFoundException;

    void addMembers(List<Long> membersId, Long id) throws RecordNotFoundException;

    void removeMembers(List<Long> membersId, Long id) throws RecordNotFoundException;

    void addTeamLeader(Long teamLeaderId, Long id) throws RecordNotFoundException;
    
    void addMember(Long teamId, Long userId) throws RecordNotFoundException;
    
    List<TeamEntity> getTeamsByTeamLead(Long id);
    
}
