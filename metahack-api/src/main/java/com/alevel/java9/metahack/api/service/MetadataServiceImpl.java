package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.MetadataEntity;
import com.alevel.java9.metahack.api.repository.MetadataRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MetadataServiceImpl implements MetadataService {

    private final MetadataRepository metadataRepository;

    @Override
    public MetadataEntity getMetadataById(Long id) throws RecordNotFoundException {
        return metadataRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No metadata record exist for given id"));
    }
}
