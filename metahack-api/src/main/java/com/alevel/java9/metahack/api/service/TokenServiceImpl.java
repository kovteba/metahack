package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.TokenEntity;
import com.alevel.java9.metahack.api.repository.TokenRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TokenServiceImpl implements TokenService {

    private final TokenRepository tokenRepository;

    @Override
    public TokenEntity getTokenByToken(String token) {
        return tokenRepository.findByToken(token);
    }

    @Override
    public TokenEntity createToken(TokenEntity tokenEntity) {
        TokenEntity tokenEntityFromDB = tokenRepository.findByUserId(tokenEntity.getUserId());
        if (tokenEntityFromDB == null) {
            return tokenRepository.save(tokenEntity);
        } else {
            tokenEntityFromDB.setToken(tokenEntity.getToken());
            return tokenRepository.save(tokenEntityFromDB);
        }
    }

    @Override
    public void deleteToken(String token) throws RecordNotFoundException {
        tokenRepository.delete(tokenRepository.findByToken(token));
    }

}
