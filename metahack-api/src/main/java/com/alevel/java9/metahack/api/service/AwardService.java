package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.AwardEntity;

import java.util.List;

public interface AwardService {

    List<AwardEntity> getAllAwards();

    AwardEntity getAwardById(Long id) throws RecordNotFoundException;

    AwardEntity createAward(AwardEntity entity, Long teamId, Long eventId) throws RecordNotFoundException;

    void deleteAwardById(Long id) throws RecordNotFoundException;

}
