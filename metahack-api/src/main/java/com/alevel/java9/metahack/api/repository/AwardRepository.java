package com.alevel.java9.metahack.api.repository;

import com.alevel.java9.metahack.api.model.AwardEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AwardRepository extends JpaRepository<AwardEntity, Long> {
}