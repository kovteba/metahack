package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.ProjectEntity;

public interface ProjectService {

    ProjectEntity getProjectById(Long id) throws RecordNotFoundException;

    ProjectEntity getProjectByName(String name) throws RecordNotFoundException;

    ProjectEntity createProject(ProjectEntity entity, Long teamId, Long eventId) throws RecordNotFoundException;

    ProjectEntity updateProject(ProjectEntity entity, Long id) throws RecordNotFoundException;

}
