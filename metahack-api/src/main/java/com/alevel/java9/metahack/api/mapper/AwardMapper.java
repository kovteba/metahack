package com.alevel.java9.metahack.api.mapper;

import com.alevel.java9.metahack.api.model.AwardEntity;
import com.alevel.java9.metahack.common.Award;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface AwardMapper {

    Award awardEntityToAward(AwardEntity awardEntity);

    AwardEntity awardToAwardEntity(Award award);
}
