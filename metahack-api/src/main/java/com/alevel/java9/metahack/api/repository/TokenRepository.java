package com.alevel.java9.metahack.api.repository;

import com.alevel.java9.metahack.api.model.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TokenRepository extends JpaRepository<TokenEntity, Long> {

    TokenEntity findByToken(String token);

    TokenEntity findByUserId(Long id);

}
