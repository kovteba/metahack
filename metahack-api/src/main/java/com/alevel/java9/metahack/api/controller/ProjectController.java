package com.alevel.java9.metahack.api.controller;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.mapper.ProjectMapper;
import com.alevel.java9.metahack.api.model.ProjectEntity;
import com.alevel.java9.metahack.api.service.ProjectService;
import com.alevel.java9.metahack.common.Project;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/projects")
@AllArgsConstructor
public class ProjectController {

    private final ProjectService projectService;
    private final ProjectMapper projectMapper;

    @GetMapping("/{id}")
    public Project getProjectById(@PathVariable("id") Long id)
            throws RecordNotFoundException {

        return projectMapper.projectEntityToProject(projectService.getProjectById(id));
    }

    @GetMapping("/{name}")
    public Project getProjectByName(@PathVariable("name") String name)
            throws RecordNotFoundException {

        return projectMapper.projectEntityToProject(projectService.getProjectByName(name));
    }

    @PostMapping("/{teamId}/{eventId}")
    public ResponseEntity<Project> createProject(@RequestBody Project project, @PathVariable Long teamId,
                                                 @PathVariable Long eventId) throws RecordNotFoundException {

        ProjectEntity created = projectService.createProject(projectMapper.projectToProjectEntity(project), teamId, eventId);

        return ResponseEntity.status(HttpStatus.CREATED).body(projectMapper.projectEntityToProject(created));
    }

    @PutMapping("/id")
    public Project updateProject(@RequestBody Project project, @PathVariable Long id)
            throws RecordNotFoundException {

        ProjectEntity updated = projectService.updateProject(projectMapper.projectToProjectEntity(project), id);

        return projectMapper.projectEntityToProject(updated);
    }
}
