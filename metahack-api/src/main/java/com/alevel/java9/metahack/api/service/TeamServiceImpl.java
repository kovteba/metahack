package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.EventEntity;
import com.alevel.java9.metahack.api.model.TeamEntity;
import com.alevel.java9.metahack.api.model.UserEntity;
import com.alevel.java9.metahack.api.repository.EventRepository;
import com.alevel.java9.metahack.api.repository.TeamRepository;
import com.alevel.java9.metahack.api.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class TeamServiceImpl implements TeamService {

    private final TeamRepository teamRepository;

    private final UserRepository userRepository;

    private final UserService userService;

    @Override
    public List<TeamEntity> getAllTeams() {
        return new ArrayList<>(teamRepository.findAll());
    }

    @Override
    public TeamEntity getTeamById(Long id) throws RecordNotFoundException {
        return teamRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));
    }

    @Override
    public TeamEntity createTeam(TeamEntity entity) {
        return teamRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteTeamById(Long id) throws RecordNotFoundException {
        teamRepository.findById(id).orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));

        teamRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void addMembers(List<Long> membersId, Long id) throws RecordNotFoundException {
        TeamEntity updatedTeam = teamRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));

        List<UserEntity> newMembers = userRepository.findAllById(membersId);

        updatedTeam.addMembers(newMembers);

        teamRepository.save(updatedTeam);
    }

    @Override
    @Transactional
    public void removeMembers(List<Long> membersId, Long id) throws RecordNotFoundException {
        TeamEntity updatedTeam = teamRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));

        List<UserEntity> newMembers = userRepository.findAllById(membersId);

        updatedTeam.removeMembers(newMembers);

        teamRepository.save(updatedTeam);
    }

    @Override
    @Transactional
    public void addTeamLeader(Long teamLeaderId, Long id) throws RecordNotFoundException {
        TeamEntity updatedTeam = teamRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));

        updatedTeam.setTeamLeader(userService.getUserById(teamLeaderId));

        teamRepository.save(updatedTeam);
    }
    
    @Override
    public void addMember(Long teamId, Long userId) throws RecordNotFoundException  {
        TeamEntity updatedTeam = teamRepository.findById(teamId)
                .orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));
        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new RecordNotFoundException("No user record exist for given id"));
        updatedTeam.getMembers().add(userEntity);
        teamRepository.save(updatedTeam);
    }
    
    @Override
    public List<TeamEntity> getTeamsByTeamLead(Long id) {
        return teamRepository.getAllByTeamLeaderId(id);
    }

}
