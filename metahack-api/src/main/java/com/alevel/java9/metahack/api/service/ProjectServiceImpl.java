package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.EventEntity;
import com.alevel.java9.metahack.api.model.ProjectEntity;
import com.alevel.java9.metahack.api.model.TeamEntity;
import com.alevel.java9.metahack.api.repository.EventRepository;
import com.alevel.java9.metahack.api.repository.ProjectRepository;
import com.alevel.java9.metahack.api.repository.TeamRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final TeamRepository teamRepository;
    private final EventRepository eventRepository;

    @Override
    public ProjectEntity getProjectById(Long id) throws RecordNotFoundException {
        Optional<ProjectEntity> project = projectRepository.findById(id);

        return project.orElseThrow(() -> new RecordNotFoundException("No project record exist for given id"));
    }

    @Override
    public ProjectEntity getProjectByName(String name) throws RecordNotFoundException {
        Optional<ProjectEntity> project = projectRepository.findByName(name);

        return project.orElseThrow(() -> new RecordNotFoundException("No project record exist for given name"));
    }

    @Override
    @Transactional
    public ProjectEntity createProject(ProjectEntity project, Long teamId, Long eventId) throws RecordNotFoundException {

        TeamEntity team = teamRepository.findById(teamId)
                .orElseThrow(() -> new RecordNotFoundException("No team record exist for given id"));
        EventEntity event = eventRepository.findById(eventId)
                .orElseThrow(() -> new RecordNotFoundException("No event record exist for given id"));

        project.setTeam(team);
        project.setEvent(event);
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public ProjectEntity updateProject(ProjectEntity entity, Long id) throws RecordNotFoundException {
        projectRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No project record exist for given id"));

        return projectRepository.save(entity);
    }
}
