package com.alevel.java9.metahack.api.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false, unique = true)
    private String phoneNumber;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String personalInfo;

    @ManyToMany(mappedBy = "members", fetch = FetchType.EAGER)
    private List<TeamEntity> teams;

    @OneToMany(mappedBy = "organizer")
    private List<EventEntity> events;

    @OneToOne(mappedBy = "teamLeader")
    private TeamEntity team;

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private RoleEntity role;

    public UserEntity() {
    }

    public UserEntity(String login, String password, String firstName, String lastName, String email, String phoneNumber, String personalInfo) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.personalInfo = personalInfo;
    }
}
