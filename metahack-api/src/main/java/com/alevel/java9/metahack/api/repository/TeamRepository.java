package com.alevel.java9.metahack.api.repository;

import com.alevel.java9.metahack.api.model.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeamRepository extends JpaRepository<TeamEntity, Long> {
    
    List<TeamEntity> getAllByTeamLeaderId(Long id);
    
}
