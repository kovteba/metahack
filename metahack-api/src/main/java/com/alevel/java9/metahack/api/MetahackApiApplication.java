package com.alevel.java9.metahack.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetahackApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MetahackApiApplication.class, args);

    }
}
