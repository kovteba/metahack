package com.alevel.java9.metahack.api.mapper;

import com.alevel.java9.metahack.api.model.EventEntity;
import com.alevel.java9.metahack.common.Event;
import org.mapstruct.*;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        uses = {MetadataMapper.class, AwardMapper.class, ProjectMapper.class})
public interface EventMapper {

    @Mappings({
            @Mapping(source = "metadata", target = "metadataList"),
            @Mapping(source = "awards", target = "awardList"),
            @Mapping(source = "projects", target = "projectList")
    })
    Event eventEntityToEvent(EventEntity eventEntity);

    EventEntity eventToEventEntity(Event event);
}
