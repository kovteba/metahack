package com.alevel.java9.metahack.api.service;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.model.TagEntity;

public interface TagService {

    TagEntity getTagById(Long id) throws RecordNotFoundException;
}
