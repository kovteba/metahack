package com.alevel.java9.metahack.api.controller;

import com.alevel.java9.metahack.api.exception.RecordNotFoundException;
import com.alevel.java9.metahack.api.mapper.TeamMapper;
import com.alevel.java9.metahack.api.mapper.UserMapper;
import com.alevel.java9.metahack.api.model.TeamEntity;
import com.alevel.java9.metahack.api.service.TeamServiceImpl;
import com.alevel.java9.metahack.common.Team;
import com.alevel.java9.metahack.common.User;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/teams")
@AllArgsConstructor
public class TeamController {

    private final TeamServiceImpl teamService;
    private final TeamMapper teamMapper;
    private final UserMapper userMapper;

    @GetMapping
    public List<Team> getAllTeams() {

        return teamService.getAllTeams()
                .stream()
                .map(teamMapper::teamEntityToTeam)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Team getTeamById(@PathVariable("id") Long id)
            throws RecordNotFoundException{

        return teamMapper.teamEntityToTeam(teamService.getTeamById(id));
    }

    @GetMapping("/{id}/members")
    public List<User> getTeamMembersByTeamId(@PathVariable Long id) throws RecordNotFoundException {
        TeamEntity team = teamService.getTeamById(id);
        return team.getMembers()
                .stream()
                .map(userMapper::userEntityToUser)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<Team> createTeam(@RequestBody Team team) {

        TeamEntity created = teamService.createTeam(teamMapper.teamToTeamEntity(team));

        return ResponseEntity.status(HttpStatus.CREATED).body((teamMapper.teamEntityToTeam(created)));
    }

    @PatchMapping("/{id}/members")
    public void addMembers(@RequestBody List<Long> membersId, @PathVariable Long id)
            throws RecordNotFoundException {

        teamService.addMembers(membersId, id);
    }

    @PostMapping("/{id}/members")
    public void removeMembers(@RequestBody List<Long> membersId, @PathVariable Long id)
            throws RecordNotFoundException {

        teamService.removeMembers(membersId, id);
    }

    @PatchMapping("/{id}/teamLeader")
    public void addTeamLeader(@RequestBody Long teamLeaderId, @PathVariable Long id)
            throws RecordNotFoundException {
        teamService.addMembers(Collections.singletonList(teamLeaderId), id);
        teamService.addTeamLeader(teamLeaderId, id);
    }

    @DeleteMapping("/{id}")
    public void deleteTeamById(@PathVariable("id") Long id)
            throws RecordNotFoundException {

        teamService.deleteTeamById(id);
    }
    
    @RequestMapping("/{teamId}/{userId}")
    public void addMember(@PathVariable Long teamId, @PathVariable Long userId)
            throws RecordNotFoundException {
        teamService.addMember(teamId, userId);
    }
    
    @GetMapping("/lead/{id}")
    public List<Team> getTeamsByTeamLead(@PathVariable Long id){
        return teamService.getTeamsByTeamLead(id)
                .stream()
                .map(teamMapper::teamEntityToTeam)
                .collect(Collectors.toList());
    }
}
