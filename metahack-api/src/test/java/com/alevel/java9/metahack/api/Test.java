package com.alevel.java9.metahack.api;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Test {


    public static void main(String[] args) {

        String date = "2020-02-29";
        String time = "12:00";
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(date + "T" + time + "+00:00[" + ZonedDateTime.now().getZone() + "]");


        System.out.println(zonedDateTime.toLocalDate().minusDays(1).equals(ZonedDateTime.now().toLocalDate()));

        System.out.println(zonedDateTime);


        String s = "http://localhost:8081/events/event/";
        Long s1 = Long.valueOf(1);

        System.out.println(s + s1);
    }

}
