$(document).ready(function () {

    //////// WORK WITH FORM LOG IN ////////
    // OPEN FORM FOR LOG IN
    $(".logInLink").click(function () {
        $(".logIn").fadeIn("fast");
        $(".logInLink").fadeOut("fast");

        $(".createEventLink").fadeOut("fast");
        $(".registrationLink").fadeOut("fast");
        $("section").css("filter", "blur(5px)");
    });
    //CLOSE FORM LOG IN
    $(".exitFromLogIn").click(function () {
        $(".logIn").fadeOut("fast");
        $(".logInLink").fadeIn("fast");

        $(".createEventLink").fadeIn("fast");
        $(".registrationLink").fadeIn("fast");
        $("section").css("filter", "none");
    });
    //////// END WORK WITH FORM LOG IN ////////

    //////// WORK WITH FORM REGISTRATION ////////
    // OPEN FORM FOR REGISTRATION
    $(".registrationLink").click(function () {
        $(".registration").fadeIn("fast");
        $(".registrationLink").fadeOut("fast");

        $(".logInLink").fadeOut("fast");
        $(".createEventLink").fadeOut("fast");
        $("section").css("filter", "blur(5px)");
    });
    //CLOSE FORM REGICTRATION
    $(".exitFromRegistration").click(function () {
        $(".registration").fadeOut("fast");
        $(".registrationLink").fadeIn("fast");

        $(".logInLink").fadeIn("fast");
        $(".createEventLink").fadeIn("fast");
        $("section").css("filter", "none");
    });
    //////// END WORK WITH FORM REGISTRATION ////////

    //////// WORK WITH FORM CREATE EVENT ////////
    // OPEN FORM CREATE EVENT
    $(".createEventLink").click(function () {
        $(".createEvent").fadeIn("fast");
        $(".createEventLink").fadeOut("fast");

        $(".logInLink").fadeOut("fast");
        $(".registrationLink").fadeOut("fast");
        $("section").css("filter", "blur(5px)");
    });
    //CLOSE FORM CREATE EVENT
    $(".exitFromCreateEvent").click(function () {
        $(".createEvent").fadeOut("fast");
        $(".createEventLink").fadeIn("fast");

        $(".logInLink").fadeIn("fast");
        $(".registrationLink").fadeIn("fast");
        $("section").css("filter", "none");
    });
    //////// END WORK WITH FORM CREATE EVENT ////////


/////////////////////////////////////////////////////////////////////////////////////

    //////// CREATE USER ////////
    // $(".formRegistration").submit(function (e) {
    //     alert("alert user 1");
    //     var loginInput = document.getElementById("login").value;
    //     var password = document.getElementById("password").value;
    //     var firstName = document.getElementById("firstName").value;
    //     var lastName = document.getElementById("lastName").value;
    //     var email = document.getElementById("email").value;
    //     var phoneNumber = document.getElementById("phoneNumber").value;
    //     var personalInfo = document.getElementById("personalInfo").value;
    //     var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    //     alert("alert user 2");
    //     if (password.length < 4) {
    //         alert("alert user 3");
    //         alert("Enter correct password")
    //     } else if (reg.test(email) === false) {
    //         alert('Enter correct e-mail');
    //     } else {
    //         alert("alert user 4");
    //         var user = {
    //             "login": loginInput,
    //             "password": password,
    //             "firstName": firstName,
    //             "lastName": lastName,
    //             "email": email,
    //             "phoneNumber": phoneNumber,
    //             "personalInfo": personalInfo
    //         };
    //         alert("alert user 5");
    //         e.preventDefault();
    //         alert("alert user 6");
    //         $.ajax({
    //             type: $(this).attr('method'),
    //             url: "/user/create",
    //             // data: $(this).serialize(),
    //             data: JSON.stringify(user),
    //             // async: false,
    //             contentType: "application/json",
    //             success: function (result) {
    //
    //                 alert("User success created");
    //             }
    //         });
    //         alert(user.email + user.firstName);
    //         alert("alert user 7");
    //     }
    // });
    //////// END CREATE USER ////////

///////////////////////////////////////////////////////////////////////////////////

    //////// CREATE EVENT ////////
    $(".formCreateEvent").submit(function (e) {
        alert("alert event");
        var theme = document.getElementById("theme").value;
        var location = document.getElementById("location").value;
        var startDate = document.getElementById("startDate").value;
        var startTime = document.getElementById("startTime").value;
        var finishDate = document.getElementById("finishDate").value;
        var finishTime = document.getElementById("finishTime").value;
        var maxTeamsQty = document.getElementById("maxTeamsQty").value;
        var maxTeamParticipantsQty = document.getElementById("maxTeamParticipantsQty").value;
        var description = document.getElementById("description").value;
        var event = {
            theme: theme,
            description: description,
            location: location,
            maxTeamsQty: maxTeamsQty,
            maxTeamParticipantsQty: maxTeamParticipantsQty,
            startDateTime: startTime + "," + startDate,
            endDateTime: finishTime + "," + finishDate,
        };
        // e.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: "/createEvent",
            // data: $(this).serialize(),
            data: JSON.stringify(event),
            // async: false,
            contentType: "application/json",
            success: function (result) {
                alert("Event success created");
            }
        });
    });
    //////// END CREATE EVENT ////////

///////////////////////////////////////////////////////////////////////////

    //////// WORK WITH FORM CHANGE EVENT ////////
    // OPEN FORM CREATE EVENT
    $(".changeEventInfoLink").click(function () {
        $(".createEventForChangeInfo").fadeIn("fast");

        $(".createEventLink").fadeOut("fast");
        $(".logInLink").fadeOut("fast");
        $(".registrationLink").fadeOut("fast");
        $("section").css("filter", "blur(5px)");
    });
    //CLOSE FORM CREATE EVENT
    $(".exitFromChangeEvent").click(function () {
        $(".createEventForChangeInfo").fadeOut("fast");

        $(".createEventLink").fadeIn("fast");
        $(".logInLink").fadeIn("fast");
        $(".registrationLink").fadeIn("fast");
        $("section").css("filter", "none");
    });
    //////// END WORK WITH FORM CHANGE EVENT ////////

    //////// CHANGE EVENT ////////
    $(".formChangeEvent").submit(function (e) {
        confirm("CHANGE EVENT");
        var theme = document.getElementById("changeTheme").value;
        var location = document.getElementById("changeLocation").value;
        var startDate = document.getElementById("changeStartDate").value;
        var startTime = document.getElementById("changeStartTime").value;
        var finishDate = document.getElementById("changeFinishDate").value;
        var finishTime = document.getElementById("changeFinishTime").value;
        var maxTeamsQty = document.getElementById("changeMaxTeamsQty").value;
        var maxTeamParticipantsQty = document.getElementById("changeMaxTeamParticipantsQty").value;
        var description = document.getElementById("changeDescription").value;
        var event = {
            theme: theme,
            description: description,
            location: location,
            maxTeamsQty: maxTeamsQty,
            maxTeamParticipantsQty: maxTeamParticipantsQty,
            startDateTime: startTime + "," + startDate,
            endDateTime: finishTime + "," + finishDate,
        };
        e.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            // type: "put",
            url: $(this).attr('action'),
            // data: $(this).serialize(),
            data: JSON.stringify(event),
            // async: false,
            contentType: "application/json",
            success: function (result) {
                alert("Event success created");
            }
        });
    });
    //////// END CHANGE EVENT ////////












    // $(document).ready(function () {
    //     alert("1")
    //     $.ajax({
    //         url: "/lessTwoDay",
    //         type: "get",
    //         dataType: "JSON",
    //         success: function (response) {
    //             alert("2");
    //             var len = response.length;
    //             alert("3")
    //             if (len === 0) {
    //                 alert("Not found")
    //             } else {
    //                 for (var i = 0; i < len; i++) {
    //                     alert("4");
    //                     var id = response[i].id;
    //                     var theme = response[i].theme;
    //                     var description = response[i].description;
    //                     var location = response[i].location;
    //                     var maxTeamsQty = response[i].maxTeamsQty;
    //                     var maxTeamParticipantsQty = response[i].maxTeamParticipantsQty;
    //                     var startDateTime = response[i].startDateTime;
    //                     var endDateTime = response[i].endDateTime;
    //
    //                     var tr_str = "<tr>" +
    //                         "<td align='center'>" + id + "</td>" +
    //                         "<td align='center'>" + theme + "</td>" +
    //                         "<td align='center'>" + description + "</td>" +
    //                         "<td align='center'>" + location + "</td>" +
    //                         "<td align='center'>" + maxTeamsQty + "</td>" +
    //                         "<td align='center'>" + maxTeamParticipantsQty + "</td>" +
    //                         "<td align='center'>" + startDateTime + "</td>" +
    //                         "<td align='center'>" + endDateTime + "</td>" +
    //                         "</tr>";
    //
    //                     $("#userTable tbody").append(tr_str);
    //                     $(".userTable tbody").append(tr_str);
    //                 }
    //             }
    //         }
    //     });
    // });


});
    